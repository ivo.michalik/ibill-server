# ibill-server

## Table of Contents

  * [Installation](#installation)
  * [Usage](#usage)
  * [Docker](#docker)
  * [Tests](#tests)

### Installation

Prerequisites:
 * node - at least 8.9.1
 * redis - version 4

We recommend using `yarn` for installing dependencies and running scripts, but you can also use `npm`.

Instructions:
1. Install dependencies - `yarn install` or `npm install`.
2. Add `development.ts` config in `src/configs/` directory. U can inspire by `development.example.ts`.
3. Call build script by `yarn build` or `npm run build`.

### Usage
To start server, simply call `yarn start` or `npm run start` and server will start.

For development, there is better script called `start-develop` which uses `nodemon` for automatic restarting while editing sources. Don't forget to also call `yarn tsc` for typescript compiler or simply start watcher `yarn watch`.
 
### Docker
For easier usage, u can also use docker. There is example `docker-compose.yml` file in the root of the project. Just edit the path of project volume on your machine a simply call `docker-compose up -d` and you are ready to go.

### Tests
Project has unit and integration tests. Just call `yarn test` or `npm run test` for all tests. Or you can call inidividual tests, there are specified scripts in `package.json`.
