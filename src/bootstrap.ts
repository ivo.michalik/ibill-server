import {createExpressServer, useContainer} from "routing-controllers";
import {Container} from "typedi";
import {Application} from "express";
import {Configurator} from "./tools/Configurator";
import {RedisStorage} from "./services/RedisStorage";
import {FileStorage} from "./services/FileStorage";

export async function bootstrap(onStart?: Function): Promise<void> {
    useContainer(Container);

    const config: any = Configurator.getConfig(__dirname + "/configs/");
    Container.set("config", config);

    Container.set("trackDB", await new FileStorage(config.trackDB).open());
    Container.set("redisDB", await new RedisStorage(config.redisDB).connect());

    const app: Application = createExpressServer({
        controllers: [__dirname + "/controllers/**/*.js"],
    });
    Container.set("app", app);

    app.listen(config.server.port, () => {
        if (onStart) onStart(app, config.server);
    });
}
