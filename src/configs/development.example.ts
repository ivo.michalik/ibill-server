export = {
    server: {
        port: 3000,
    },
    redisDB: {
        host: "localhost",
        port: 6379,
    },
    trackDB: {
        directory: process.cwd() + "/runtime/",
        fileName: "log.txt",
    },
}
