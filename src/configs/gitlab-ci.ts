export = {
    server: {
        port: 3000,
    },
    redisDB: {
        host: "redis",
        port: 6379,
    },
    trackDB: {
        directory: process.cwd() + "/runtime/",
        fileName: "log.txt",
    },
}
