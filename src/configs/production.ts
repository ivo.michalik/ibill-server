export = {
    server: {
        port: 3000,
    },
    redisDB: {
        host: "example.com",
        port: 6379,
    },
    trackDB: {
        directory: process.cwd() + "/runtime/",
        fileName: "log.txt",
    },
}
