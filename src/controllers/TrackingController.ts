import {Body, Get, JsonController, Post} from "routing-controllers";
import {Inject} from "typedi";
import {RedisStorage} from "../services/RedisStorage";
import {FileStorage} from "../services/FileStorage";

@JsonController()
export class TrackingController {

    @Inject("trackDB")
    public trackDB: FileStorage;

    @Inject("redisDB")
    public redisDB: RedisStorage;

    @Get("/count")
    public async count(): Promise<object> {
        const count = await this.redisDB.connection.get("count");
        return {count: parseInt(count || "0")};
    }

    @Post("/track")
    public async track(@Body() body: any): Promise<object> {
        await this.trackDB.appendData(body);

        if (body.count) {
            await this.redisDB.connection.incrby("count", body.count);
        }

        return {status: "ok"};
    }

}
