import "reflect-metadata";
import {bootstrap} from "./bootstrap";
import {Application} from "express";

require("source-map-support").install();

bootstrap((app: Application, config: any) => {
    console.log(`Server is running available at http://${config.host || 'localhost'}:${config.port}`);
});
