import * as fs from "fs";
import * as path from "path";

export interface Configuration {
    directory: string;
    fileName: string;
}

export class FileStorage {

    protected _config: Configuration;

    protected _file: number;

    constructor(config: Configuration) {
        if (!config.fileName) throw new Error(`Filename is empty.`);
        if (!fs.existsSync(config.directory)) throw new Error(`Directory '${config.directory}' doesn't exists.`);
        this._config = config;
    }

    public get data(): string {
        return fs.readFileSync(this.filePath).toString();
    }

    public get file(): number {
        return this._file;
    }

    public get filePath(): string {
        return path.join(this._config.directory, this._config.fileName);
    }

    public appendData(data: any): Promise<void> {
        return new Promise((resolve, reject) => {
            const row: string = JSON.stringify(data) + "\n";
            fs.appendFile(this._file, row, (err) => {
                if (err) reject(err);
                else resolve();
            });
        });
    }

    public open(): Promise<this> {
        return new Promise((resolve, reject) => {
            fs.open(this.filePath, "a+", (err, file) => {
                if (err) {
                    reject(err);
                } else {
                    this._file = file;
                    resolve(this);
                }
            });
        });
    }

    public close(): Promise<void> {
        return new Promise((resolve, reject) => {
            fs.close(this._file, (err) => {
                if (err) reject(err);
                else resolve();
            });
        });
    }

}
