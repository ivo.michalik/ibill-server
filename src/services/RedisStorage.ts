import * as IORedis from "ioredis";

export class RedisStorage {

    protected _config: IORedis.RedisOptions = null;

    protected _connection: IORedis.Redis = null;

    constructor(config: IORedis.RedisOptions) {
        this._config = config;
    }

    public get connection(): IORedis.Redis {
        return this._connection;
    }

    public async connect(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._connection = new IORedis(this._config);
            this._connection.on("connect", () => {
                resolve(this);
            });
            this._connection.on("error", (err: Error) => {
                reject(err);
            });
        })
    }

    public disconnect(): void {
        this._connection.disconnect();
    }

}
