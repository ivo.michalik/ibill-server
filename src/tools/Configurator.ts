export class Configurator {

    public static getConfig(configDir: string) {
        const env: string = process.env.NODE_ENV || "development";
        return require(`${configDir}/${env}`);
    }

}
