import * as assert from "assert";
import {TrackingController} from "../../../src/controllers/TrackingController";
import {Configurator} from "../../../src/tools/Configurator";
import {RedisStorage} from "../../../src/services/RedisStorage";
import {FileStorage} from "../../../src/services/FileStorage";

let controller: TrackingController;

describe("Integration - TrackingController", () => {
    before(async () => {
        const config = Configurator.getConfig(__dirname + "/../../../src/configs/");

        controller = new TrackingController();
        controller.trackDB = await new FileStorage(config.trackDB).open();
        controller.redisDB = await new RedisStorage(config.redisDB).connect();

        await controller.redisDB.connection.del("count");
    });
    it("track", (done) => {
        const body = {data: "data"};
        controller.track(body).then((value) => {
            const data = controller.trackDB.data.split("\n");
            data.pop();
            const lastRow = data.pop();
            assert.deepStrictEqual(lastRow, JSON.stringify(body));
            assert.deepStrictEqual(value, {status: "ok"});
            done();
        });
    });
    it("track (10k times)", (done) => {
        const body = {data: "data"};
        const tracks = [];

        for (let i = 0; i < 10000; i++) {
            tracks.push(controller.track(body));
        }

        Promise.all(tracks).then((value) => {
            done();
        });
    });
    it("count (cleared redis)", (done) => {
        controller.count().then((value) => {
            assert.deepStrictEqual(value, {count: 0});
            done();
        });
    });
    it("count (1000)", (done) => {
        const body = {data: "data", count: 1000};
        controller.track(body).then(() => {
            return controller.count();
        }).then((value) => {
            assert.deepStrictEqual(value, {count: 1000});
            done();
        });
    });
    after(async () => {
        await controller.redisDB.disconnect();
        await controller.trackDB.close();
    });
});
