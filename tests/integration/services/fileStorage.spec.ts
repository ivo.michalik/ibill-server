import * as fs from "fs";
import * as path from "path";
import * as assert from "assert";
import {FileStorage} from "../../../src/services/FileStorage";

const testDir = process.cwd() + "/tests/integration/_assets/";
const testFile = "log.txt";
const testPath = path.join(testDir, testFile);

let storage: FileStorage;

describe("Integration - FileStorage", () => {
    it("constructor", (done) => {
        assert.doesNotThrow(() => {
            if (fs.existsSync(testPath)) fs.unlinkSync(testPath);

            storage = new FileStorage({directory: testDir, fileName: testFile});

            storage.open().then(() => {
                assert.strictEqual(typeof storage.file !== "undefined", true);
                done();
            });
        });
    });
    it("get - file", () => {
        assert.strictEqual(typeof storage.file !== "undefined", true);
    });
    it("get - data", () => {
        assert.strictEqual(storage.data, "");
    });
    it("appendData", (done) => {
        storage.appendData({data: "data"}).then(value => {
            const content = fs.readFileSync(testPath).toString();
            assert.deepStrictEqual(content, '{"data":"data"}' + "\n");
            done();
        });
    });
    it("get - data (after append)", () => {
        assert.strictEqual(storage.data, '{"data":"data"}' + "\n");
    });
    it("close", (done) => {
        storage.close().then(() => {
            done();
        });
    });
    after(() => {
        fs.unlinkSync(testPath);
    });
});
