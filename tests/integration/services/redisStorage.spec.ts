import * as assert from "assert";
import {RedisStorage} from "../../../src/services/RedisStorage";
import {Configurator} from "../../../src/tools/Configurator";

let storage: RedisStorage;

describe("Integration - RedisStorage", () => {
    it("constructor", () => {
        const config = Configurator.getConfig(__dirname + "/../../../src/configs/");
        storage = new RedisStorage(config.redisDB);
        assert.deepStrictEqual(storage["_config"], config.redisDB);
    });
    it("connect", async () => {
        await storage.connect();
        assert.strictEqual(storage.connection !== null, true);
    });
    it("access to DB", async () => {
        await storage.connection.set("key", "key");
        const value = await storage.connection.get("key");
        assert.strictEqual(value, "key");
    });
    it("disconnect", async () => {
        await storage.disconnect();
    });
});
