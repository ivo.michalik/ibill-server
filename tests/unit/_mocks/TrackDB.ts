import {FileStorage} from "../../../src/services/FileStorage";

class TrackDB {

    public data = null;

    appendData(data: any): any {
        return this.data = data;
    }

}

export const trackDB: FileStorage = Object.create(new TrackDB());
