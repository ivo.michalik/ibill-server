import * as assert from "assert";
import {redisDB} from "../_mocks/RedisDB";
import {trackDB} from "../_mocks/TrackDB";
import {TrackingController} from "../../../src/controllers/TrackingController";

const controller = new TrackingController();

describe("Unit - TrackingController", () => {
    before(() => {
        controller.trackDB = trackDB;
        controller.redisDB = redisDB;
    });
    it("count", (done) => {
        controller.count().then((value) => {
            assert.deepStrictEqual(value, {count: 0});
            done();
        });
    });
    it("track", (done) => {
        const body = {data: "data"};
        controller.track(body).then((value) => {
            assert.deepStrictEqual(controller.trackDB["data"], body);
            assert.deepStrictEqual(value, {status: "ok"});
            done();
        });
    });
    it("track (with count)", (done) => {
        const body = {data: "data", count: 10};
        controller.track(body).then((value) => {
            assert.deepStrictEqual(controller.redisDB["data"], 10);
            assert.deepStrictEqual(controller.trackDB["data"], body);
            assert.deepStrictEqual(value, {status: "ok"});
            done();
        });
    });
});
