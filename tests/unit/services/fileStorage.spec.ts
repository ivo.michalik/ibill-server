import * as path from "path";
import * as assert from "assert";
import {FileStorage} from "../../../src/services/FileStorage";

const testDir = __dirname + "/../_assets/";
const testFile = "log.txt";
const testPath = path.join(testDir, testFile);

let storage: FileStorage;

describe("Unit - FileStorage", () => {
    it("constructor (bad filename)", () => {
        assert.throws(() => {
            storage = new FileStorage({directory: "", fileName: ""});
        });
    });
    it("constructor (bad directory)", () => {
        assert.throws(() => {
            storage = new FileStorage({directory: "", fileName: testFile});
        });
    });
    it("constructor", () => {
        assert.doesNotThrow(() => {
            const config = {directory: testDir, fileName: testFile};
            storage = new FileStorage(config);
            assert.strictEqual(storage["_config"], config);
        });
    });
    it("get - filePath", () => {
        assert.strictEqual(storage.filePath, testPath);
    });
});
