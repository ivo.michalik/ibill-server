import * as assert from "assert";
import {RedisStorage} from "../../../src/services/RedisStorage";

let storage: RedisStorage;

describe("Unit - RedisStorage", () => {
    it("constructor", () => {
        const config = {host: "host"};
        storage = new RedisStorage(config);
        assert.strictEqual(storage["_config"], config);
    });
    it("get - connection", () => {
        assert.strictEqual(storage.connection, null);
    });
});
