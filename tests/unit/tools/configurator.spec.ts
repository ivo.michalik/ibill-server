import {Configurator} from "../../../src/tools/Configurator";
import * as assert from "assert";

const path = __dirname + "/../_assets/";

describe("Unit - Configurator", () => {
    it("getConfig (development config)", () => {
        const config = Configurator.getConfig(path);
        assert.deepStrictEqual(config, {
            url: "localhost"
        });
    });
    it("getConfig (production config)", () => {
        process.env.NODE_ENV = "production";
        const config = Configurator.getConfig(path);
        assert.deepStrictEqual(config, {
            url: "www.example.com"
        });
    });
    it("getConfig (missing config)", () => {
        process.env.NODE_ENV = "ci";
        assert.throws(() => {
            return Configurator.getConfig(path);
        }, "Cannot find module");
    });
});
